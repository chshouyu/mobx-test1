import React from 'react';
import { render } from 'react-dom';
import Store from './store';
import App from './app';

const initStore = localStorage.getItem('store-test-list') && JSON.parse(localStorage.getItem('store-test-list')) || [];

const store = Store.fromJS(initStore || []);

// 监听函数，任何改变自动存储到localStorage
store.storeToLocalStorage();

render(<App store={store} />, document.querySelector('#root'));