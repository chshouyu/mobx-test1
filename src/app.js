import React, { Component } from 'react';
import { observer } from 'mobx-react';
import DevTools from 'mobx-react-devtools';
import V4 from 'uuid/v4';
import Item from './item';

function getRandomTitle() {
  return `Title ${V4().split('-')[0]}`;
};

@observer
class App extends Component {
  render() {
    const {
      addItem,
      list
    } = this.props.store;
    return (
      <div>
        <button onClick={addItem.bind(null, getRandomTitle())}>add item</button>
        {list.map(item => <Item key={item.id} item={item} />)}
        <DevTools />
      </div>
    );
  }
};

export default App;
