import React, { Component } from 'react';
import { observer } from 'mobx-react';

@observer
export default class Item extends Component {
  render() {
    const { item } = this.props;
    return (
      <div>
        <span
          style={{color: item.done ? 'red' : 'blue', cursor: 'default'}}
          onClick={item.toggle}>
          {item.title}
        </span>
        <span onClick={item.remove} style={{cursor: 'pointer'}}> remove</span>
      </div>
    );
  }
}